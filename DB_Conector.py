import MySQLdb

class DBConector():

    def __init__(self, host, name, user, password):
        print 'Conect for database'
        self.db = MySQLdb.connect(host, user, password, name)
        self.name = name
        self.cursor = self.db.cursor()
        print 'Conected'

    def insert(self, table_name, **kwargs):
        """Insert into database"""
        keys = []
        values = []
        str_key = ""
        str_values = ""
        string1 = ""
        string2 = ""
        for key in kwargs:            
            string1 = ''.join(["`", str(key), "`"])
            keys.append(string1)
            string2 = ''.join([" \" ", kwargs[key], " \" "])
            values.append(string2)
        str_key = ','.join(keys)
        str_values = ','.join(values)

        print str_key
        print str_values

        self.cursor.execute("""INSERT INTO `%s`.`%s`(%s)VALUES (%s); """ %(self.name, table_name, str_key, str_values))
        self.db.commit()

    def update(self, table_name, id, **kwargs):
        """Update into database"""
        str_value = []
        string_ = ""
        for key in kwargs:
            str_value.append(string_.join([" `", key, "`= \"", kwargs[key], "\" "]))
        str_value = ','.join(str_value)
        self.cursor.execute("""UPDATE `%s`.`%s` SET %s WHERE
         `id`=%s; """ %(self.name, table_name, str_value, id))
        self.db.commit()
        return True

    def select_all(self, table_name):
        """Select all from table"""
        self.cursor.execute(""" SELECT * FROM  `%s`.`%s` ;""" %(self.name, table_name))
        return self.cursor.fetchmany(50)

    def select_id(self,table_name, id):
        """Select one pfgbc from table"""
        self.cursor.execute(""" SELECT * FROM  `%s`.`%s` WHERE `id`= %s ; """ %(self.name, table_name, id))
        return self.cursor.fetchmany()


    def delete(self, table_name, id):
        """Detele one pfgbc from database """
        self.cursor.execute(""" DELETE FROM  `%s`.`%s` WHERE `id`= %s ; """ %(self.name, table_name, id))
        self.db.commit()
        return "Contact with id %s deleted"%(id)




