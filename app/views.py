from flask import render_template, flash, redirect
from app import app
from forms import LoginForm

@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"

# @app.route('/<name>')

@app.route('/login', methods = ['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Name="' + form.name.data + '", Email=' + str(form.email.data))
        return redirect('/index')
    return render_template('login.html', 
        title = 'Add user',
        form = form)