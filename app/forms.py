from flask.ext.wtf import Form
from wtforms import TextField, BooleanField
from wtforms.validators import Required

class LoginForm(Form):
    name = TextField('name', validators = [Required()])
    alias = TextField('alias', validators = [Required()])
    email = TextField('email', validators = [Required()])
    password = TextField('password', validators = [Required()])
    repit_password = TextField('repit_password', validators = [Required()])
    avatar = TextField('avatar', validators = [Required()])    
    accept_terms = BooleanField('Accept terms', default = False)