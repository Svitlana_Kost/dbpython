from DB_Conector import DBConector
from Mongo_Conector import Mongo_Conector
DB_HOST = 'localhost'
DB_USER = 'jeffrey'
DB_PASSWORD = 'userp'
DB_NAME = 'dbContacts'

class Contact():

    name = []
    last_name = []
    telephon = []
    company = []
    city = []

    def __init__(self, dbCon, db=""):
        self.db = db
        self.DB = dbCon 
        if self.db == "mysql":            
            self.table_name = "Contacts"
        elif self.db == "mongo":
            self.table_name = "contacts"
            


    def print_(self, res):
        list_results = []
        for i in range(len(res)):
            if res[i] == None:
                list_results.append("")
            else:
                list_results.append(res[i])
        print "-------------------------------%s\n " %(list_results[0])
        print "Contact: %s %s \n " %(list_results[1], list_results[2])
        print "Telephon: %s \n " %(list_results[3])
        print " %s %s\n "%(list_results[5], list_results[4])
        print "-----------------------------------"

    def input_contact(self):

        self.name = ['name', str(raw_input("Name: "))]
        self.telephon = ["telephon", raw_input("Telephon: ")]
        self.last_name = ["last_name", raw_input("Surname: ")]
        self.company = ["company", raw_input("Company: ")]
        self.city = ["city", raw_input("City: ")]
        list_ = [self.name, self.telephon, self.last_name, self.company, self.city]
        results = dict(list_)
        for result in list_:
            if result[1] == "":
                del results[result[0]]
        return results


    def add(self):
        print 'Input...'
        res = self.input_contact()
        print res
        self.DB.insert(self.table_name, **res)
        self.menu()

    def get(self):
        print 'Choice oper\n 1 - Get all\n 2 - Get by id'
        inp = raw_input("You choice: ")
        if inp == '1':
            results = self.DB.select_all(self.table_name)
        else:
            id = raw_input("Id: ")
            results = self.DB.select_id(self.table_name, id)
        if results:
            for result in results:
                self.print_(result)
        else:
            print 'Empty list'
        self.menu()

    def update(self):
        id = raw_input("Id: ")
        res = self.DB.select_id(self.table_name, id)
        self.print_(res[0])
        dic = self.input_contact()
        self.DB.update(self.table_name, id, **dic)
        res = self.DB.select_id(self.table_name, id)
        self.print_(res[0])
        self.menu()


    def remove(self):
        id = raw_input("Id: ")
        result = self.DB.delete(self.table_name, id)
        print result
        self.menu()


    def exit(self):
        print 'Goodbye!'

    def menu(self):
        operations = {'1':self.add, '2':self.get, '3':self.update, '4':self.remove, '5':self.exit}
        print 'Choice oper \n 1 - Add\n 2 - Get\n 3 - Update\n 4 - Remove\n 5 - Exit'
        var = raw_input("Input ")
        print 'You choice:', var
        if operations.get(var):
            print '\x1b[1;32;40m', 'OK', '\x1b[0m'
            operations.get(var)()
        else:
            print '\x1b[1;31;40m', 'Error', '\x1b[0m'
            self.menu()

if __name__ == "__main__":
    print 'Choice DB:\n 1 - MySql \n 2 - Mongo'
    id_db = raw_input("Input: ")
    if id_db == '1':
        db = "mysql"
        DBCon = DBConector(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD)

    elif id_db == '2':
        db = "mongo"
        DBCon = Mongo_Conector(DB_HOST)
    table = Contact(DBCon, db)
    table.menu()
    